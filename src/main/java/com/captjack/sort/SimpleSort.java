package com.captjack.sort;

/**
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2022/10/16 22:31
 * package com.captjack.sort
 */
public class SimpleSort {

    /**
     * 插入排序
     * 保持前i个元素有序，对第i+1个元素只需找到他对应第位置交换即可
     *
     * @param arr
     */
    public static void insertSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j] >= arr[j - 1]) {
                    break;
                }
                swap(arr, j, j - 1);
            }
        }
    }

    /**
     * 每次将最大的元素冒到数组尾部
     *
     * @param arr
     */
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap(arr, j + 1, j);
                }
            }
        }
    }

    /**
     * 每次选择后面最小的元素与当前元素交换
     *
     * @param arr
     */
    public static void selectSort(int[] arr) {
        int minIndex;
        for (int i = 0; i < arr.length; i++) {
            minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                swap(arr, minIndex, i);
            }
        }
    }

    public static void swap(int[] arr, int s, int t) {
        int temp = arr[s];
        arr[s] = arr[t];
        arr[t] = temp;
    }

}
