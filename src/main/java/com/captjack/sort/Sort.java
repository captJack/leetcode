package com.captjack.sort;

/**
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2022/10/11 12:26
 * package com.captjack.docker.simple.application
 */
public class Sort {

    private static int num;

    public static void main(String[] args) {

    }

    public int aroundCenter(String s, int left, int right) {
        while (left >= 0 && right <= s.length() - 1 && s.charAt(left) == s.charAt(right)) {
            left--;
            right++;
        }
        return right - left - 1;
    }

    public static long combine(int total, int switchCount) {
        long proc = 1, div = 1;
        for (int i = 0; i < switchCount; i++) {
            proc *= total - i;
            div *= i + 1;
        }
        return proc / div;
    }

}
