package main

import "fmt"

func main() {
	fmt.Println(123)
}

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}
