package com.captjack;

/**
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2022/10/17 20:54
 * package com.captjack
 */
public class Test {

    public static void main(String[] args) {
    }

    public int minimumDeletions(int[] nums) {
        int minIndex = 0, maxIndex = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > nums[maxIndex]) {
                maxIndex = i;
            }
            if (nums[i] < nums[minIndex]) {
                minIndex = i;
            }
        }
        int left = Math.min(maxIndex, minIndex), right = Math.max(maxIndex, minIndex);
        //
        return Math.min(Math.min(right + 1, nums.length - left), nums.length - right + left + 1);
    }

    /**
     * // 转度数       原坐标         转换坐标
     * 90°            [x, y]   ->    [y, n-x]
     * 180°           [x, y]   ->    [n-x, n-y]
     * 270°           [x, y]   ->    [n-y, x]
     */
    public boolean findRotation(int[][] mat, int[][] target) {
        boolean right90 = true, right180 = true, right270 = true, right360 = true;
        //
        int n = mat.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                right90 = right90 && mat[i][j] == target[j][n - i - 1];
                right180 = right180 && mat[i][j] == target[n - i - 1][n - j - 1];
                right270 = right270 && mat[i][j] == target[n - j - 1][i];
                right360 = right360 && mat[i][j] == target[i][j];
            }
        }
        return right90 || right180 || right270 || right360;
    }

}

