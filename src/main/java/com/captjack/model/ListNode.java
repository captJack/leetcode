package com.captjack.model;

/**
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2022/10/17 22:15
 * package com.captjack.model
 */
public class ListNode {

    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

}

