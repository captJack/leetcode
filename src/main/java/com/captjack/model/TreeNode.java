package com.captjack.model;

/**
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2022/10/17 22:14
 * package com.captjack.model
 */
public class TreeNode {

    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

}