from typing import List


class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:
        pass


def quick_power(a, power):
    res = 1.0
    while power > 0:
        if power & 1 == 1:
            res *= a
        a *= a
        power >>= 1
    return res


if __name__ == '__main__':
    print()
